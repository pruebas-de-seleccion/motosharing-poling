#MOTOSHARING POLING

Una prueba de concepto para una api de actualizaciones de estado para recursos de motosharing a base de poling.

### Prerequisites

- Lombok plugin: Para ahorrar código innecesario como getter y setter se añadido [lombok](https://projectlombok.org/) al proyecto. En el caso de no querer instalarlo en tu IDE te recomiendo ejecutar el [comando delombok](https://projectlombok.org/features/delombok) que genera el codigo y elimina las anotaciones.

### Implementación
He realizado una implementacion muy básica ya que no he podido dedicarle el tiempo necesario y conocia la premura de vuestro proceso. Me he centrado en dejar claro el workflow de trabajo que uso en el día a día a base de mis commit en git y un codigo limpio que pueda ser una base para futuros trabajos.

####¿Que podeis ver?

- He realizado un trabajo exploratorio sobre la solución. No he usado DDD pero he realizado un minimo de separacion por capas. El objetivo de esto era mostrar la posibilidad de hacer un desarrollo rapido mvp pero de forma limpia y rapidamente ampliable. De esta manera, con una solución funcionando y cubierta de test funcionales se puede empezar una refactorización para sustituir las piezas o mejorar la performance de forma segura.
  - StatusChangeChecker es una de las piezas angulares del diseño de esta solucion. Es el encargado de comprobar los cambios entre actualizaciones. El trabajo se hace en memoria pero en el [apartado de mejoras](#mejoras) hablamos sobre ello.
  - [Documentación en swagger](localhost:8080/swagger-ui.html)
  - Almaccenamiento del ultimo estado conocido de los recursos.
  - Sistema asincrono de actualizaciones para nuevos recursos, recursos actualizados y recursos fuera de servicio (hablaremos de ello en el [apartado de mejoras](#mejoras))
  - StatusUpdaterTrigger.java es unicamente el trigger del proceso de actualizacion, de esta manera podemos actualizarlo de forma facil sin afectar a la logica.

- Solo he implementado un endpoint que te proporciona el ultimo estado concocido del sistema. Pero he dejado el sistema preparado para poder implemtar un endpoint reactivo de forma facil con el que podríamos estar informados de cualquier actualización una vez obtenido el estado actual.


### Preguntas propuestas:

#### ¿Cómo de escalable es tu solución propuesta? 
La solucion propuesta no es mas que un esqueleto pero puede ser muy estable con las implementaciones adecuadas. Además en primera instancia es facilmente escalable con unos pequeños cambios. 

Podríamos esclar por ejemplo por zonas, haciendo que la configuracion de la query de localizacion se obtuviera de forma remota por cada instancia. De esa forma los microservicios se encargarian de actualizar las distintas zonas de forma independiente e iterativa. Como todos los microservicios alimentarían de forma asincrona por eventos (kafka o rabbitMQ) el mismo sistema de notificaciones seria muy sencillo mantener un datastorage centralizado por ciudades por ejemplo o por grupos de zonas.


#### ¿Que problemas a futuro podría presentar? Si has detectado alguno. ¿Qué alternativa/s propones para solventar dichos problemas?
- En la solucion propuesta no se ha implementado monitorizacion ni control de errores. Usaría Datadog para las metricas y alertas y ELK stack para los log (datadog es caro para almacenar log)
- No se ha deserializado la informacion del resource, esto nos crea una dependencia con el formato que de la api. Si fuera una api de terceros deberíamos atajar esto rapidamente. Almacenar solo los campos necesarios y definir nuestro propio formato de deserializacion sería la solución mas acertada a priori.   
- El soporte de la solucion para el StatusChangeChecker puede ser muy complicado, dependiendo de la solucion aportada, pero necesitariamos mucho performance en este punto.
- Debemos realizar test de contrato contra la api que usamos para consumir datos, de esta manera podríamos enterarnos de primera mano ante un cambio y actuar.

### <a name="mejoras"></a>Proximos pasos 
- Redis para la deteccion de cambios:
  - He estado barajando la utilizacion de Redis para este proceso. De hecho la solucion esta diseñada para ello basada en las funciones de redis para detectad diferencias en set de datos. Son operaciones con alta performance. La idéa principal sería mantener un set de Id de resources por query.
  
- Implementar un sistema de notificaciones que permita propagar los eventos a otros microservios, con kafka o rabitMQ. Esto permitiria tener el storage de stado distribuido en un cluster por ejemplo o actualizarse de forma automatica solo de los recursos que entren o salgan de ciertas zonas.

- Refactorizar usando DDD para separarnos del framework

- Separar la capa api de la de dominio. Actualmente la entidad de dominio está anotada para serialización en json.

 