package com.jorgejbarra.meep.motosharingpoling.repository.remote;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.notFound;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.serverError;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.OK;

@AutoConfigureWireMock
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class FeignMeepResourcesApiClientShould {

  @Autowired
  private FeignMeepResourcesApiClient client;

  @Test
  public void retrieveMeepStatusOfResourcesFromExpectedEndpoint() {
    //given
    final String expectedResponseBody = "{mocked response for expected request}";
    stubFor(get(urlEqualTo("/tripplan/api/v1/routers/lisboa/resources?"
        + "lowerLeftLatLon=38.711046,-9.160096"
        + "&upperRightLatLon=38.739429,-9.137115"
        + "&companyZoneIds=545,467,473"))
        .willReturn(ok()
            .withHeader("Content-Type", "application/json;charset=UTF-8")
            .withBody(expectedResponseBody)));

    //when
    final ResponseEntity<String> statusResponse = client.status(
        "lisboa",
        "38.711046,-9.160096",
        "38.739429,-9.137115",
        "545,467,473"
    );

    //then
    assertThat(statusResponse.getStatusCode()).isEqualTo(OK);
    assertThat(statusResponse.getBody()).isEqualTo(expectedResponseBody);
  }

  @Test(expected = feign.FeignException.class)
  public void throwsExceptionWhenStatusIs500() {
    //given
    stubFor(get(anyUrl())
        .willReturn(serverError()));

    //when
    client.status("lisboa", "38.711046,-9.160096", "38.739429,-9.137115", "545,467,473");
  }

  @Test(expected = feign.FeignException.class)
  public void throwsExceptionWhenStatusIs400() {
    //given
    stubFor(get(anyUrl())
        .willReturn(notFound()));

    //when
    client.status("lisboa", "38.711046,-9.160096", "38.739429,-9.137115", "545,467,473");
  }
}
