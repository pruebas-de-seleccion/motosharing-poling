package com.jorgejbarra.meep.motosharingpoling.repository.remote;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;

import static com.jorgejbarra.meep.motosharingpoling.Utils.resourceId;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StatusRemoteRepositoryShould {

  private FeignMeepResourcesApiClient apiClient = mock(FeignMeepResourcesApiClient.class);
  private ResourcesQuery properties = new ResourcesQuery(
      "test-zona",
      "test-left",
      "test-right",
      "test-zone-ids"
  );

  private final StatusRemoteRepository repository = new StatusRemoteRepository(properties, apiClient);

  @Test(expected = MeepApiError.class)
  public void throwApiExceptionWhenClientDoNotReturn200() {
    //Given
    mockServerResponseTo(ResponseEntity.badRequest().build());

    //When
    repository.fetch();
  }

  @Test
  public void deserializeResponse() throws IOException {
    //Given
    mockServerResponseTo(ResponseEntity.ok(getFileContentAsString("example.json")));

    //When
    final Set<Resource> fetchedValues = repository.fetch();

    //Then
    assertThat(fetchedValues).hasSize(1)
        .first().extracting("id").isEqualTo(resourceId("DriveNow_13673363"));
  }

  private String getFileContentAsString(final String fileName) throws IOException {
    final String filePath = ResourceUtils.getFile("classpath:" + fileName).getPath();
    return new String(Files.readAllBytes(Paths.get(filePath)));
  }

  private OngoingStubbing<ResponseEntity<String>> mockServerResponseTo(final ResponseEntity<String> expectedResponse) {
    return when(apiClient.status(properties.getZone(), properties.getLowerLeftLatLon(), properties.getUpperRightLatLon(), properties.getCompanyZoneIds())).thenReturn(expectedResponse);
  }
}