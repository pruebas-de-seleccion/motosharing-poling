package com.jorgejbarra.meep.motosharingpoling;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.repository.CurrentStatusRepository;
import com.jorgejbarra.meep.motosharingpoling.repository.InMemoryCurrentStatusRepository;
import java.util.Set;
import org.junit.Test;

import static com.jorgejbarra.meep.motosharingpoling.Utils.resource;
import static com.jorgejbarra.meep.motosharingpoling.Utils.setOf;
import static org.assertj.core.api.Assertions.assertThat;

public class StatusChangesCheckerShould {

  private CurrentStatusRepository repository = new InMemoryCurrentStatusRepository();
  private final StatusChangesChecker checker = new StatusChangesChecker(repository);

  @Test
  public void returnAddedValuesWhenCheckNews() {
    //given
    setCurrentStatus(setOf(resource("Sample-1")));
    final Set<Resource> values = setOf(resource("Sample-3"));

    //when
    final Set<Resource> newResources = checker.checkNews(values);

    //then
    assertThat(newResources).contains(resource("Sample-3"));
  }

  @Test
  public void notReturnOldValuesWhenCheckNews() {
    //given
    setCurrentStatus(setOf(resource("Sample-1")));
    final Set<Resource> values = setOf(resource("Sample-3"));

    //when
    final Set<Resource> newResources = checker.checkNews(values);

    //then
    assertThat(newResources).doesNotContain(resource("Sample-1"));
  }

  @Test
  public void notReturnDuplicateValuesWhenCheckNews() {
    //given
    setCurrentStatus(setOf(resource("Sample-1"), resource("Sample-2")));
    final Set<Resource> values = setOf(resource("Sample-3"), resource("Sample-2"));

    //when
    final Set<Resource> newResources = checker.checkNews(values);

    //then
    assertThat(newResources).doesNotContain(resource("Sample-2"));
  }

  @Test
  public void notReturnNewValuesWhenCheckOutOfService() {
    //given
    setCurrentStatus(setOf(resource("Sample-1")));
    final Set<Resource> values = setOf(resource("Sample-3"));

    //when
    final Set<Resource> lostValues = checker.checkOutOfService(values);

    //then
    assertThat(lostValues).doesNotContain(resource("Sample-3"));
  }

  @Test
  public void notReturnDuplicatedValuesWhenCheckOutOfService() {
    //given
    setCurrentStatus(setOf(resource("Sample-1"), resource("Sample-2")));
    final Set<Resource> values = setOf(resource("Sample-3"), resource("Sample-2"));

    //when
    final Set<Resource> lostValues = checker.checkOutOfService(values);

    //then
    assertThat(lostValues).doesNotContain(resource("Sample-2"));
  }

  @Test
  public void returnLostValuesWhenCheckOutOfService() {
    //given
    setCurrentStatus(setOf(resource("Sample-1"), resource("Sample-2")));
    final Set<Resource> values = setOf(resource("Sample-3"), resource("Sample-2"));

    //when
    final Set<Resource> lostValues = checker.checkOutOfService(values);

    //then
    assertThat(lostValues).containsExactly(resource("Sample-1"));
  }

  @Test
  public void returnUpdatedResources() {
    //Given
    setCurrentStatus(setOf(resource("Sample-10", "old value")));
    final Resource updatedResource = resource("Sample-10", "updated");
    final Set<Resource> values = setOf(updatedResource, resource("Sample-2"));

    //when
    final Set<Resource> updates = checker.checkUpdated(values);

    //Then
    assertThat(updates).containsExactly(updatedResource);
  }

  @Test
  public void notReturnNoUpdatedResources() {
    //Given
    final Resource noUpdatedResource = resource("Sample-10", "updated");
    setCurrentStatus(setOf(noUpdatedResource, resource("Sample-2")));
    final Set<Resource> values = setOf(noUpdatedResource);

    //when
    final Set<Resource> updates = checker.checkUpdated(values);

    //Then
    assertThat(updates).isEmpty();
  }

  private void setCurrentStatus(final Set<Resource> initialValues) {
    repository.setStatus(initialValues);
  }
}
