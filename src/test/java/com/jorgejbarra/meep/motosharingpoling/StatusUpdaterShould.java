package com.jorgejbarra.meep.motosharingpoling;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceValue;
import com.jorgejbarra.meep.motosharingpoling.eventbus.UpdatedNotifier;
import com.jorgejbarra.meep.motosharingpoling.repository.CurrentStatusRepository;
import com.jorgejbarra.meep.motosharingpoling.repository.InMemoryCurrentStatusRepository;
import com.jorgejbarra.meep.motosharingpoling.repository.remote.StatusRemoteRepository;
import java.util.Set;
import org.junit.Test;

import static com.jorgejbarra.meep.motosharingpoling.Utils.resource;
import static com.jorgejbarra.meep.motosharingpoling.Utils.setOf;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StatusUpdaterShould {

  private final CurrentStatusRepository currentStatusRepository = new InMemoryCurrentStatusRepository();
  private final StatusChangesChecker updatesChecker = new StatusChangesChecker(currentStatusRepository);

  private final StatusRemoteRepository statusRemoteRepository = mock(StatusRemoteRepository.class);
  private final UpdatedNotifier updatedNotifier = mock(UpdatedNotifier.class);

  private final StatusUpdater statusUpdater = new StatusUpdater(currentStatusRepository, updatesChecker, statusRemoteRepository, updatedNotifier);

  @Test
  public void updateCurrentStatusWithNewStatus() {
    //Given
    currentStatusRepository.setStatus(setOf(
        new Resource(new ResourceId("id-1"), new ResourceValue("{}")),
        new Resource(new ResourceId("id-2"), new ResourceValue("{ to delete}"))
    ));

    final Set<Resource> newStatus = setOf(
        new Resource(new ResourceId("id-1"), new ResourceValue("{ updated }")),
        new Resource(new ResourceId("id-3"), new ResourceValue("{ new }"))
    );
    when(statusRemoteRepository.fetch()).thenReturn(newStatus);

    //When
    statusUpdater.execute();

    //Then
    assertThat(currentStatusRepository.getStatus()).containsExactlyInAnyOrderElementsOf(newStatus);
  }

  @Test
  public void sendNotificationForNewResources() {
    //Given
    currentStatusRepository.setStatus(emptySet());

    final Resource newResource = new Resource(new ResourceId("id-1"), new ResourceValue("{ new }"));
    when(statusRemoteRepository.fetch()).thenReturn(setOf(newResource));

    //When
    statusUpdater.execute();

    //Then
    verify(updatedNotifier).notifyNew(newResource);
  }

  @Test
  public void sendNotificationForOutOfServiceResources() {
    //Given
    final Resource deletedResource = resource("id-1");
    currentStatusRepository.setStatus(setOf(deletedResource));

    when(statusRemoteRepository.fetch()).thenReturn(emptySet());

    //When
    statusUpdater.execute();

    //Then
    verify(updatedNotifier).notifyOutOfService(deletedResource);
  }

  @Test
  public void sendNotificationForOutOfUpdatedResources() {
    //Given
    currentStatusRepository.setStatus(setOf(resource("id-2", "{old value}")));

    final Resource updatedResource = resource("id-2", "{updated}");
    when(statusRemoteRepository.fetch()).thenReturn(setOf(updatedResource));

    //When
    statusUpdater.execute();

    //Then
    verify(updatedNotifier).notifyUpdated(updatedResource);
  }
}
