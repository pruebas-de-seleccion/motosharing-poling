package com.jorgejbarra.meep.motosharingpoling;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceValue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class Utils {

  @SafeVarargs
  static <T> Set<T> setOf(final T... resources) {
    return new HashSet<>(Arrays.asList(resources));
  }

  public static ResourceId resourceId(final String value) {
    return new ResourceId(value);
  }

  static Resource resource(final String id) {
    return new Resource(new ResourceId(id), new ResourceValue("{}"));
  }

  static Resource resource(final String id, final String value) {
    return new Resource(new ResourceId(id), new ResourceValue(value));
  }
}
