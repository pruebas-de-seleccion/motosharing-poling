package com.jorgejbarra.meep.motosharingpoling.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ResourceValue {
  @Getter
  private String value;
}
