package com.jorgejbarra.meep.motosharingpoling.domain;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Resource {

  @JsonUnwrapped
  private final ResourceId id;
  @JsonUnwrapped
  private final ResourceValue value;
}
