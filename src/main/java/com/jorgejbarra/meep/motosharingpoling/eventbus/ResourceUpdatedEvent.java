package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;

public class ResourceUpdatedEvent extends ResourceEvent {
  ResourceUpdatedEvent(final Object source, final Resource resource) {
    super(source, resource);
  }
}