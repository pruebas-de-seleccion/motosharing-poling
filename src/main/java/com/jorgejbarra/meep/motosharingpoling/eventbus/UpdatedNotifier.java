package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;

public interface UpdatedNotifier {
  void notifyNew(Resource newResource);

  void notifyOutOfService(Resource deletedResource);

  void notifyUpdated(Resource updatedResource);
}
