package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ApplicationEventUpdatedNotifier implements UpdatedNotifier {

  private ApplicationEventPublisher applicationEventPublisher;

  @Override
  public void notifyNew(final Resource newResource) {
    applicationEventPublisher.publishEvent(new NewResourceEvent(this, newResource));
  }

  @Override
  public void notifyOutOfService(final Resource deletedResource) {
    applicationEventPublisher.publishEvent(new ResourceOutOfServiceEvent(this, deletedResource));
  }

  @Override
  public void notifyUpdated(final Resource updatedResource) {
    applicationEventPublisher.publishEvent(new ResourceUpdatedEvent(this, updatedResource));
  }
}
