package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;

public class ResourceOutOfServiceEvent extends ResourceEvent {
  ResourceOutOfServiceEvent(final Object source, final Resource resource) {
    super(source, resource);
  }
}
