package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;

public class NewResourceEvent extends ResourceEvent {
  NewResourceEvent(final Object source, final Resource resource) {
    super(source, resource);
  }
}
