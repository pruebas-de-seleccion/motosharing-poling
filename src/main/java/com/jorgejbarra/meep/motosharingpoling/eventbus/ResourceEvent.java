package com.jorgejbarra.meep.motosharingpoling.eventbus;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

abstract class ResourceEvent extends ApplicationEvent {
  @Getter
  private final Resource resource;

  ResourceEvent(final Object source, final Resource resource) {
    super(source);
    this.resource = resource;
  }
}
