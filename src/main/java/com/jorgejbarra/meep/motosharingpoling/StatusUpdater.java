package com.jorgejbarra.meep.motosharingpoling;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.eventbus.UpdatedNotifier;
import com.jorgejbarra.meep.motosharingpoling.repository.CurrentStatusRepository;
import com.jorgejbarra.meep.motosharingpoling.repository.remote.StatusRemoteRepository;
import java.util.Set;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StatusUpdater {
  private final CurrentStatusRepository currentStatusRepository;
  private final StatusChangesChecker updatesChecker;
  private final StatusRemoteRepository statusRemoteRepository;
  private final UpdatedNotifier updatedNotifier;

  public void execute() {
    final Set<Resource> updatedStatus = statusRemoteRepository.fetch();

    checkNews(updatedStatus);
    checkOutOfService(updatedStatus);
    checkUpdated(updatedStatus);

    currentStatusRepository.setStatus(updatedStatus);
  }

  private void checkUpdated(final Set<Resource> updatedStatus) {
    final Set<Resource> newResources = updatesChecker.checkUpdated(updatedStatus);
    for (Resource resource : newResources) {
      updatedNotifier.notifyUpdated(resource);
    }
  }

  private void checkOutOfService(final Set<Resource> updatedStatus) {
    final Set<Resource> newResources = updatesChecker.checkOutOfService(updatedStatus);
    for (Resource resource : newResources) {
      updatedNotifier.notifyOutOfService(resource);
    }
  }

  private void checkNews(final Set<Resource> updatedStatus) {
    final Set<Resource> newResources = updatesChecker.checkNews(updatedStatus);
    for (Resource resource : newResources) {
      updatedNotifier.notifyNew(resource);
    }
  }
}
