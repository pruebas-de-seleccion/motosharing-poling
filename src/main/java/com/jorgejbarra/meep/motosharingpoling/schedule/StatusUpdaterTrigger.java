package com.jorgejbarra.meep.motosharingpoling.schedule;

import com.jorgejbarra.meep.motosharingpoling.StatusUpdater;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@AllArgsConstructor
public class StatusUpdaterTrigger {
  private final Logger logger = LoggerFactory.getLogger(StatusUpdaterTrigger.class);
  private final StatusUpdater statusUpdater;

  @Scheduled(initialDelay = 6000, fixedRate = 6000)
  public void reportCurrentTime() {
    logger.trace("Triggered execution of status updater");
    statusUpdater.execute();
  }
}