package com.jorgejbarra.meep.motosharingpoling.api;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.repository.CurrentStatusRepository;
import java.util.Set;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CurrentStatusController {
  private final CurrentStatusRepository repository;

  @GetMapping("/resources")
  public ResponseEntity<Set<Resource>> getStatus() {
    return ResponseEntity.ok(repository.getStatus());
  }
}
