package com.jorgejbarra.meep.motosharingpoling;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import com.jorgejbarra.meep.motosharingpoling.repository.CurrentStatusRepository;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StatusChangesChecker {

  private final CurrentStatusRepository repository;

  public Set<Resource> checkNews(final Set<Resource> values) {
    final Set<ResourceId> currentStatusId = repository.getStatus().stream().map(Resource::getId).collect(Collectors.toSet());
    return new HashSet<>(values).parallelStream().filter(
        r -> !currentStatusId.contains(r.getId())
    ).collect(Collectors.toSet());
  }

  public Set<Resource> checkOutOfService(final Set<Resource> values) {
    final Set<ResourceId> valuesId = values.stream().map(Resource::getId).collect(Collectors.toSet());
    return new HashSet<>(repository.getStatus()).parallelStream().filter(
        r -> !valuesId.contains(r.getId())
    ).collect(Collectors.toSet());
  }

  Set<Resource> checkUpdated(final Set<Resource> values) {
    return new HashSet<>(values).parallelStream().filter(
        updated -> repository.get(updated.getId())
            .map(
                original -> !original.getValue().equals(updated.getValue())
            ).orElse(false)
    ).collect(Collectors.toSet());
  }
}
