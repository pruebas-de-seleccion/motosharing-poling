package com.jorgejbarra.meep.motosharingpoling.repository;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import java.util.Optional;
import java.util.Set;

public interface CurrentStatusRepository {
  Set<Resource> getStatus();

  void setStatus(Set<Resource> resources);

  Optional<Resource> get(ResourceId id);
}
