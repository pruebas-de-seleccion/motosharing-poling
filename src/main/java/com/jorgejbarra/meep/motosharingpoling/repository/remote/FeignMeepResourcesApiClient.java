package com.jorgejbarra.meep.motosharingpoling.repository.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "meep-resources", url = "${meep-api.base-url}")
interface FeignMeepResourcesApiClient {

  @RequestMapping(method = RequestMethod.GET, value = "/routers/{zone}/resources")
  ResponseEntity<String> status(
      @PathVariable("zone") final String zone,
      @RequestParam(value = "lowerLeftLatLon") final String lowerLeftLatLon,
      @RequestParam(value = "upperRightLatLon") final String upperRightLatLon,
      @RequestParam(value = "companyZoneIds") final String companyZoneIds
  );
}
