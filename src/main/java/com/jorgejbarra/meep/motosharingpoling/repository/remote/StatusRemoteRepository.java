package com.jorgejbarra.meep.motosharingpoling.repository.remote;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceValue;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
@AllArgsConstructor
@EnableConfigurationProperties(ResourcesQuery.class)
public class StatusRemoteRepository {

  private final ResourcesQuery resourcesQuery;
  private final FeignMeepResourcesApiClient apiClient;
  private final ObjectMapper mapper = new ObjectMapper();

  public Set<Resource> fetch() {
    final ResponseEntity<String> response = apiClient.status(
        resourcesQuery.getZone(),
        resourcesQuery.getLowerLeftLatLon(),
        resourcesQuery.getUpperRightLatLon(),
        resourcesQuery.getCompanyZoneIds()
    );

    if (response.getStatusCode().isError()) {
      throw new MeepApiError(format("Status %d :: %s", response.getStatusCode().value(), response.getBody()));
    }

    return asListOfJsonValues(response.getBody())
        .map(
            jsonVal -> new Resource(new ResourceId(getIdQuietly(jsonVal)), new ResourceValue(jsonVal))
        ).collect(Collectors.toSet());
  }

  private Stream<String> asListOfJsonValues(final String value) {
    try {
      final Iterator<JsonNode> sourceIterator = mapper.readTree(value).iterator();
      return StreamSupport.stream(((Iterable<JsonNode>) () -> sourceIterator).spliterator(), false).parallel()
          .map(JsonNode::toPrettyString);
    } catch (Exception e) {
      throw new IllegalArgumentException("Can not read array of resources from: " + value, e);
    }
  }

  private String getIdQuietly(final String jsonVal) {
    try {
      return mapper.readTree(jsonVal).get("id").asText();
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Can not read id of resource: " + jsonVal, e);
    }
  }
}
