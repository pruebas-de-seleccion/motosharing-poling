package com.jorgejbarra.meep.motosharingpoling.repository.remote;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Getter
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "resources-query")
class ResourcesQuery {
  private final String zone;
  private final String lowerLeftLatLon;
  private final String upperRightLatLon;
  private final String companyZoneIds;
}
