package com.jorgejbarra.meep.motosharingpoling.repository.remote;

public class MeepApiError extends RuntimeException {
  MeepApiError(final String message) {
    super(message);
  }
}
