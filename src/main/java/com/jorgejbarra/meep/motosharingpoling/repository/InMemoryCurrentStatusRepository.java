package com.jorgejbarra.meep.motosharingpoling.repository;

import com.jorgejbarra.meep.motosharingpoling.domain.Resource;
import com.jorgejbarra.meep.motosharingpoling.domain.ResourceId;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Repository;

import static java.util.Collections.unmodifiableSet;

@Repository
public class InMemoryCurrentStatusRepository implements CurrentStatusRepository {
  private final Set<Resource> storage = new HashSet<>();

  @Override
  public Set<Resource> getStatus() {
    return unmodifiableSet(storage);
  }

  @Override
  public void setStatus(final Set<Resource> resources) {
    storage.clear();
    storage.addAll(resources);
  }

  @Override
  public Optional<Resource> get(final ResourceId id) {
    return storage.parallelStream().filter(r -> r.getId().equals(id)).findAny();
  }
}
